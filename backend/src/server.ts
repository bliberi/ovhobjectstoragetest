import * as dotenv from 'dotenv';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { Client as MinioClient } from 'minio';

dotenv.config();

const {
    MINIO_KEY = 'minio_key',
    MINIO_SECRET = 'minio_secret',
    MINIO_ENDPOINT = 'localhost',
    MINIO_PORT = '9000',
    MINIO_SSL = 'false',
    MINIO_REGION = 'us-east-1',
    PORT = '4000',
} = process.env;

const minioClient = new MinioClient({
    endPoint: MINIO_ENDPOINT,
    port: parseInt(MINIO_PORT, 10),
    useSSL: MINIO_SSL === 'true',
    accessKey: MINIO_KEY,
    secretKey: MINIO_SECRET,
    region: MINIO_REGION,
});

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/presignedUrl', async (req, res) => {
    const [bucket, ...rest] = req.body.resource.split('/');
    const result = await minioClient
        .presignedGetObject(bucket, rest.join('/'))
        .then((value) => value)
        .catch(() => null);
    res.send(result);
});

app.listen(PORT, () => {
    console.log(`server is running on ${PORT}`);
});
