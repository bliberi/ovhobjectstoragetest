import axios from 'axios';
import {
    Button,
    Card,
    Col,
    Descriptions,
    Form,
    Input,
    Row,
    Space,
    message,
} from 'antd';
import React from 'react';

function App() {
    const [presignedUrl, setPresignedUrl] = React.useState<string>('');

    const handleDownload = React.useCallback((attachmentUrl: string) => {
        fetch(attachmentUrl)
            .then((response) => response.blob())
            .then((blob) => {
                const link = document.createElement('a');
                link.id = 'link';
                link.href = URL.createObjectURL(blob);
                link.download = 'download';
                link.click();
                URL.revokeObjectURL(link.href);
                link.remove();
            })
            .catch(() => {
                message.error('downloadError');
            });
    }, []);

    const getPresignedUrl = React.useCallback((values: any) => {
        const { resource } = values;
        axios
            .post(process.env.REACT_APP_BACKEND + '/presignedUrl', { resource })
            .then((res) => setPresignedUrl(res.data));
    }, []);

    return (
        <Space direction='vertical' size='middle' style={{ width: '100%' }}>
            <Form onFinish={getPresignedUrl}>
                <Row gutter={12}>
                    <Col span={22}>
                        <Form.Item name='resource'>
                            <Input placeholder='resource name... (eg. bucketname/filename.png' />
                        </Form.Item>
                    </Col>
                    <Col span={2}>
                        <Button htmlType='submit'>Search</Button>
                    </Col>
                </Row>
            </Form>

            <Descriptions>
                <Descriptions.Item label='Presigned Url'>
                    {presignedUrl}
                </Descriptions.Item>
            </Descriptions>
            <Card
                hoverable
                style={{ width: 240 }}
                cover={
                    presignedUrl ? (
                        <img alt='' src={presignedUrl} />
                    ) : (
                        <p>insert resource and click 'search'</p>
                    )
                }
            >
                <Button
                    onClick={() => handleDownload(presignedUrl)}
                    disabled={presignedUrl === ''}
                >
                    Download
                </Button>
            </Card>
        </Space>
    );
}

export default App;
