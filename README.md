# start project

## backend

1. add `.env` from `.env.example`
2. run `yarn`
3. run `yarn start`

## frontend

1. add `.env.development` from `.env.example`
2. run `yarn`
3. run `yarn start`
